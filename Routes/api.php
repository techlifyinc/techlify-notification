<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get("notification-types", "NotificationTypeController@index");
Route::post("notification-types", "NotificationTypeController@store");
Route::put("notification-types/{notificationType}", "NotificationTypeController@update");
Route::delete("notification-types/{notificationType}", "NotificationTypeController@destroy");

Route::get("notification-subscription-entities", "NotificationSubscriptionEntityController@index");

Route::get("notification-subscriptions", "NotificationSubscriptionController@index");
Route::get("notification-subscriptions-unique", "NotificationSubscriptionController@unique");
Route::post("notification-subscriptions", "NotificationSubscriptionController@store");
Route::put("notification-subscriptions/{subscription}", "NotificationSubscriptionController@update");
Route::patch("notification-subscriptions-toggle/{subscription}", "NotificationSubscriptionController@toggle");
Route::post("notification-subscriptions-for-user", "NotificationSubscriptionController@subscribeForUser");
Route::delete("notification-subscriptions/{subscription}", "NotificationSubscriptionController@destroy");

Route::get("notifications", "NotificationController@index");

Route::get("notification-users", "NotificationUserController@index");
Route::patch("notification-user-status/{nUser}", "NotificationUserController@updateStatus");

Route::get("notification-statuses", "NotificationStatusController@index");

Route::get("notification-user-statuses", "NotificationUserStatusController@index");

Route::get("notification-user-timelines", "NotificationUserTimelineController@index");
