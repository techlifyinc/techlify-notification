<?php

namespace Modules\TechlifyNotification\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Event;
use Modules\TechlifyNotification\Events\NotificationCreatorEvent;
use Modules\TechlifyNotification\Listeners\NotificationCreatorListener;

class TechlifyNotificationServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('TechlifyNotification', 'Database/Migrations'));

        //Registering Events
        Event::listen(
            NotificationCreatorEvent::class,
            [NotificationCreatorListener::class, 'handle']
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('TechlifyNotification', 'Config/config.php') => config_path('techlifynotification.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('TechlifyNotification', 'Config/config.php'),
            'techlifynotification'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/techlifynotification');

        $sourcePath = module_path('TechlifyNotification', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/techlifynotification';
        }, \Config::get('view.paths')), [$sourcePath]), 'techlifynotification');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/techlifynotification');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'techlifynotification');
        } else {
            $this->loadTranslationsFrom(module_path('TechlifyNotification', 'Resources/lang'), 'techlifynotification');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('TechlifyNotification', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
