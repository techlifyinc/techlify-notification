<?php

namespace Modules\TechlifyNotification\Events;

use Illuminate\Queue\SerializesModels;
use Modules\TechlifyNotification\Entities\NotificationStatus;
use Modules\TechlifyNotification\Entities\NotificationType;

class NotificationCreatorEvent
{
    use SerializesModels;

    public $notification;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($notification)
    {
        $notificationType = NotificationType::getNotificationType($notification['type_code']);
        if (!$notificationType || !($notification['title'] ?? null)) {
            throw ('Invalid Notification');
        }
        $notification['type_id'] = $notificationType->id;
        $notification['status_id'] = NotificationStatus::PENDING;
        $notification['creator_id'] = auth()->id();
        $notification['client_id'] = auth()->user()->client_id;

        $this->notification = $notification;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
