<?php

namespace Modules\TechlifyNotification\Console;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Throwable;
use Modules\TechlifyNotification\Entities\Notification;
use Modules\TechlifyNotification\Entities\NotificationStatus;
use Modules\TechlifyNotification\Entities\NotificationSubscription;
use Modules\TechlifyNotification\Entities\NotificationSubscriptionEntity;
use Modules\TechlifyNotification\Entities\NotificationUser;
use Modules\TechlifyNotification\Entities\NotificationUserStatus;

class NotificationDeliveryGuy
{

    protected $queryLimit = 50;

    public function run()
    {
        try {
            $pendingNotifications = $this->getPendingNotificationsQuery();
            foreach ($pendingNotifications->get() as $notification) {
                $this->processNotification($notification);
            }
            $pendingNotifications->update(['status_id' => NotificationStatus::PROCESSED]);
        } catch (Throwable $th) {
            throw $th;
        }
    }

    function processNotification($notification)
    {
        try {
            $subscribers = $this->getSubscribersForNotification($notification);
            $notificationUsers = [];
            foreach ($subscribers as $subscriber) {
                $notificationUser = [
                    'user_id' => $subscriber->id,
                    'notification_id' => $notification->id,
                    'status_id' => NotificationUserStatus::DELIVERED,
                    'created_at' => now(),
                    'updated_at' => now()
                ];
                array_push($notificationUsers, $notificationUser);
            }
            DB::table((new NotificationUser())->getTable())->insert($notificationUsers);
        } catch (Throwable $th) {
            throw $th;
        }
    }

    function getSubscribersForNotification($notification)
    {
        try {
            if ($notification->constraints !== null) {
                //There is a constraints for this notification.
                $subscribers = $this->getSubscribersForConstraints($notification);
                return $subscribers;
            }
            $entityIds = NotificationSubscription::where('type_id', $notification->type_id)
                ->where('is_subscribed', true)
                ->select('entity_id')
                ->without('creator')
                ->distinct()
                ->pluck('entity_id');

            $subscribers = collect([]);
            foreach ($entityIds as $entityId) {
                switch ($entityId) {
                    case NotificationSubscriptionEntity::USER_TYPE:
                        $users = User::whereIn('user_type_id', $this->getSubscriberIdsForEntity($entityId, $notification))->get();
                        $subscribers = $subscribers->concat($users);
                        break;

                    case NotificationSubscriptionEntity::ROLE:
                        $users = User::whereHas('roles', function ($q) use ($entityId, $notification) {
                            $q->whereIn('roles.id', $this->getSubscriberIdsForEntity($entityId, $notification));
                        })->get();
                        $subscribers = $subscribers->concat($users);
                        break;

                    default:
                        //Individual
                        $users = User::whereIn('id', $this->getSubscriberIdsForEntity($entityId, $notification))->get();
                        $subscribers = $subscribers->concat($users);
                        break;
                }
            }
            $subscribers = $subscribers->unique('id');
            return $subscribers->values();
        } catch (Throwable $th) {
            throw $th;
        }
    }

    function getSubscribersForConstraints($notification)
    {
        if (($notification->constraints['is_user_constraint'] ?? false) && ($notification->constraints['subscriber_id'] ?? null) !== null) {
            //constraint for user
            return User::where('id', $notification->constraints['subscriber_id'])->get();
        }
        return [];
    }

    function getSubscriberIdsForEntity($entityId, $notification)
    {
        return NotificationSubscription::where('type_id', $notification->type_id)
            ->where('entity_id', $entityId)
            ->where('is_subscribed', true)
            ->pluck('subscriber_id');
    }

    function getPendingNotificationsQuery()
    {
        return Notification::where('status_id', NotificationStatus::PENDING)->limit($this->queryLimit);
    }
}
